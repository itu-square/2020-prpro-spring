{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Hamiltonian Monte Carlo Sampling (Chapter 14.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(there is no PyMC3 code in this notebook)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Metropolis Sampling\n",
    "* A quiz on www.menti.com first\n",
    "* In Metropolis we do a random walk in the parameter space, favouring positions that have higher posterior probability density\n",
    "* At each iteration a **proposal distribution** decides what next position we should consider\n",
    "* Then we accept the proposal when it improves over our current density, or thanks to some small random luck\n",
    "* The **proposal distribution is fixed** throughout the run of an algorithm, in principle, independent of the prior and likelihood.\n",
    "* The **proposal distribution is centered on the current position** (usually a multivariate Gaussian distribution)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#  Gibbs Sampling\n",
    "* A quiz on www.menti.com first\n",
    "* Uses analytical results for each marginal prior-likelihood pair to propose moves that are directly corresponding to the density change in each dimension\n",
    "* Requires **conjugate priors**\n",
    "* Moves only at **one-dimension at a time**\n",
    "* This makes it very **hard to effectively sample highly correlated narrow valleys** in the posterior\n",
    "\n",
    "![](valley.png)\n",
    "Image credits: Nikolaj Bläser"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Hamiltonian Monte-Carlo\n",
    "\n",
    "<img style=\"float: left; padding-right: 2cm;\" src=\"silly-walk.jpg\">\n",
    "\n",
    "* It is still a **random walk** algorithm (like both Metropolis and Gibbs)\n",
    "* It does not use random proposals, and does not use conjugate prior (solved analytically)\n",
    "* It exploits the local shape of the posterior curve/surface/etc.\n",
    "* Our goal is to sample a representative representation of the posterior, so if we can approximate it as much as possible directly we will proceed faster to the goal.\n",
    "  * Random proposals of Metropolis may be exploring very low probability regions\n",
    "  * Single-dimensional proposals of Gibbs may be stuck in narrow posteriors not parallel to axes\n",
    "* Hamiltonian assumes we can *approximate the gradient of the posterior* around the current sampling point\n",
    "* Then it uses physics simulation to follow steep gradients faster."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# HMC: Intuition\n",
    "\n",
    "<img style=\"float: right; padding-left: 3cm; padding-top: 0cm;\" src=\"hmc.png\">\n",
    "\n",
    "* Top row: we do not want to sample symmetrically from the two starting points. We want to skew for the distribution.\n",
    "* Let's invert the density function (negative logarithm, known as **potential**)\n",
    "* Intuition: potential is like **gravitational potential** in physics\n",
    "    * potential energy can be translated to **kinetic energy**\n",
    "* Imagine that current position is a marble (a point mass) \n",
    "    * In a limited time it will roll down the potential curve/surface, etc.\n",
    "* To introduce randomness we apply a **random momentum** and let it roll, and see where it lands.\n",
    "    * random = gaussian centered on current position\n",
    "    * This allows the gradient to **weight** the sampling.\n",
    "    * The lower we go, the higher posterior density, so we get what we need.\n",
    "* To compute the physical transfer of gravitational energy to speed, we need to know the gradient (the **differential**) of the posterior.\n",
    "  * This is why everything is based on the Theano library which represents computations that are **differentiable** (or TensorFlow)\n",
    "  * The book does not show the physical calcuation, and the intuition suffices.\n",
    "* Bottom row: the sampled 'proposal distributions'\n",
    "    * The proposal is not symmetric\n",
    "    * It works in multiple dimensions\n",
    "    * \"The marbles will follow even narrow valleys down\"\n",
    "    * The cost of each sampling is much higher, but the autocorrelation is decreasing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# HMC a few details\n",
    "\n",
    "* We would accept all proposals if calculations were exact (because the sum of kinetic and potential energy are constant)\n",
    "* Approximations of Hamiltonian simulation mean that sometimes we need to reject, as we do not want imprecisions to affect the samples.\n",
    "\n",
    "* The acceptance condition is like in the metropolis (ratio of posteriors, but corrected by the prior probability of the random momentum):\n",
    "\n",
    "$$\n",
    "p_\\mathrm{accept} = \\min \\left({{p(\\theta_\\mathrm{proposed} | D) p(\\phi_\\mathrm{proposed})}\\over{p(\\theta_\\mathrm{current} | D) p(\\phi_\\mathrm{current})}}, 1\\right)\n",
    "$$\n",
    "\n",
    "* Note that the in our simulation the sum of the logs of the terms in the numerator/denomminator is the total energy system (posterior is the potential energy, and the prior on the momentum is the kinetic energy).  \n",
    "    * In physics this sum is constant if there is no energy loss\n",
    "    * But the sum of logarithms equals to the logarithm of a multiplication, so the logarithm of the multiplication in the numerator/denominator is a constant.\n",
    "    * But if a logarithm is constant  then so is its argument.\n",
    "    * So the ratio should always be equal to 1 if we had perfect maths and perfect simulation\n",
    "    * In practice this rules catches imprecisions of calculations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# HMC: Parameters\n",
    "\n",
    "<img style=\"float: right;padding-left: 4cm;\" src=\"overshooting.png\">\n",
    "\n",
    "\n",
    "* **Epsilon** \n",
    "  * The size of the simulation step \n",
    "  * Smaller steps, in principle, mean better precision, and less rejections\n",
    "* **Steps**\n",
    "  * The number of simulation steps. \n",
    "  * These control time. \n",
    "  * Too few steps, we move too slowly; \n",
    "  * Too many steps we may overshoot the high density locations\n",
    "* **Standard deviation** of the initial momentum (usually selected automatically)\n",
    "* Rule of thumb: aim for 65% acceptance rate (the book)\n",
    "  * PyMC3 API docs recommend even higher (95%)\n",
    "* PymC3 method sample takes an argument ``target_accept: float in [0, 1]``. \n",
    "  * The step size is tuned automatically to get this acceptance rate. \n",
    "* Figure: examples of overshooting with long trajectories (steps*epsilon)\n",
    "  * Note how bad the posterior approximation is in the right column,  bottom plot\n",
    "* Note that the long trajectories make a U-turn\n",
    "    * **NUTS** = no U-turn sampler (Hoffman & Gelman, 2014) \n",
    "    * NUTS has a protection against these U-turns to improve effectiveness\n",
    "    * This is the Hamiltonian sampler we used most of the time, as it is default in PyMC3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercises\n",
    "Some exercises are taken from \"Statistical Rethinking by Richard McElreath\"\n",
    "\n",
    "1. Which kind of parameters cannot be sampled using Hamiltonian Monte Carlo? Why?\n",
    "\n",
    "2. Find the code you have implemented last week for reproducing Figure 12.1. Perhaps tweak the parameters so that the scatter plot looks similar to the one by Nikolaj above (narrow). Now sve this data and use it as training data for training a model (we are faking a situation as if this was real data).  The trained model can be the same, just make the priors weak, and add observation data.  Experiment with Metropolis, Gibbs and NUTS samplers in PyMC3 to learn posterior distributions over the paramaters $\\theta_1$ and $\\theta_2$.  Can you confirm that in this scenario the Hamiltonian sampler has much higher convergence ? (for instance higer ESS). Is it translating to faster sampling at better quality on the wall clock?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
