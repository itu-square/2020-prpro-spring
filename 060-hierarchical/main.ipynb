{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "import pymc3 as pm\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import arviz as az\n",
    "from matplotlib import pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Examples of Hierarchical Models (Chapter 9)\n",
    "\n",
    "* **Tossing coins from different mints**, mint has a typical bias, each coin has its own concrete bias (that varies a bit between coins)\n",
    "* **Baseball player abilitie**s: Each player has a success rate, a number of opportunities, and the primary position in the game. To that we add a higher layer: a typical batting ability of a player by position. (this is like a coin where every bat opportunity is a toss, every player is a coin with a certain success rate, every position is like the mint with a typical success rate for its players).\n",
    "* **Success of surgery** in different hospitals by different teams:  i) the number of recoveries per team ii) the number of surgeries per team ii) the hospital city (this is like a coin, where every operation is a toss, every team is a coin, every hospital has a typical success rate)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Single Mint Single Coin (Example #1)\n",
    "* For starters let's just consider a single coin with parameter $\\theta$ coming from a mint with a typical bias concentrated around $\\omega$. \n",
    "* The following figure summarizes the model graphically (Fig. 9.1 in Kruschke)\n",
    "\n",
    "![](single-mint-single-coin.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "* The concentration $K$ is considered constant in this model for simplicity.\n",
    "* The top-level distribution controls how different are the mints from each other.\n",
    "* The code below presents the same model in PyMC3 (where we made $A_\\omega$ and $B_\\omega$ equal to 2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "with pm.Model() as model1:\n",
    "    K = 100  # concentration, how reliable is the bias of coins produced by the mint\n",
    "    omega = pm.Beta (\"omega\", 2,2)\n",
    "    theta = pm.Beta (\"theta\", alpha=(K-2)+1, beta=(1-omega)*(K-2)+1)\n",
    "    y = pm.Bernoulli (\"y\", p=theta)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Bayes Theorem (Applied to the hierarchical case)\n",
    "\n",
    "* With hierarchical modeling we apply Bayes rule to the joint parameter space.\n",
    "* This is the Bayes rule applied to the joint parameter space:\n",
    "\n",
    "$$p (\\theta,\\omega \\mid y) \\sim p (y \\mid \\theta, \\omega) ~ p (\\theta, \\omega) = p (y \\mid \\theta) ~ p(\\theta\\mid\\omega) ~ p (\\omega) $$\n",
    "\n",
    "* The second equality is important: \n",
    "    * If we fix $\\theta$, \n",
    "    * Then the experiment result $y$ no longer depends on $\\omega$\n",
    "    * We can use the chain rules to factorize more conditional probabilities.  \n",
    "    * This way we can avoid working with a large joint probability space.  \n",
    "* Advantages of such hierarchical factorization\n",
    "    * The model structure makes sense for humans (to check that it is correct and rational)\n",
    "    * * Inference algorithms can eploit the decomposition (for instance Gibbs sampling will work if it can sample from each of the conditional likelihoods)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Hierarchical Model \n",
    "\n",
    "**Def.** *Hierarchical Model is any model that can be factored into a chain of dependencies like above* \n",
    "\n",
    "* In CS, we would take any acyclic directed graph of conditional probabilities, so we would say it is a model in the factored form.\n",
    "* In a more CS perspective, hierarchical models, AKA Bayesian nets, are compact representations of joint probability distributions based on Bayes rule.\n",
    "* In principle this decomposability depends on the parameterization  (not all parameterizations are decomposable this way)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Grid Approximation of the 1-mint 1-coin Hierarchical Model\n",
    "* The following is computed with grid approximation (NOT with the sampling model created above).\n",
    "* I took the figure from the text book\n",
    "* We can take the grid approximation approach because both $\\theta$ and $\\omega$ have finite domains (between 0..1)\n",
    "* Just take the result of experiment, and calculate the likelihood from the hierarchical model directly for a grid of $(\\theta, \\omega)$ points.\n",
    "* So we fix $y$ and for each $(\\theta,\\omega)$ pair calculate $p (y \\mid \\theta) ~ p(\\theta\\mid\\omega) ~ p (\\omega)$\n",
    "* There isn't any direct gain from the factoring yet (this could be just as efficient for: $p (y \\mid \\theta, \\omega) ~ p (\\theta, \\omega)$ if we had the formulae\\\n",
    "\n",
    "![](fig-9.2.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Grid Approximation Tricks (AKA Patterns)\n",
    "\n",
    "* This figure summarizes a few grid approximation tricks that are useful:\n",
    "    * **Trick 1**: How to convert the computed values into probability mass? Divide by the sum of values at all the points.\n",
    "    * **Trick 2**: How to convert mass into density estimates?  Divide the probability mass estimates by the surface of the grid square.\n",
    "        * For a single dimensional approximation, we divide by the width of the interval (for instance the marginal priors in the figure).\n",
    "    * **Trick 3**: To compute a marginal of the posterior (no direct definition) we can integrate (sum) the other parameter out (bottom row) and renormalize again."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "notes"
    }
   },
   "source": [
    "# Some notes and observations\n",
    "* The posterior probability on $\\theta$ (given $\\omega$) shown in the bottom right corners (small plots) has not changed a lot with respect to the prior.  This is because the prior was pretty strict (confident) regarding dependence of $\\theta$ on $\\omega$ with $K=100$.\n",
    "* On the other hand, the posterior of $\\omega$ is very different from the prior. It has shifted towards the ration of successes (6/9)\n",
    "* The following figure reverses the situation, it makes the dependence of $\\theta$ on $\\omega$ weak ($K=6$), but makes the certainty of $\\omega$ high ($A_\\omega = B_\\omega=20)$. So we know the mint very well, but we are not sure how precisely the mint produces a typical coin\n",
    "* We will skip the analysis of this figure in class (most likely) to save time.\n",
    "    * Note the much sharper prior $p(\\omega)$, which does not go through all values in the $\\omega$ domain anymore\n",
    "    * The dependence of $\\theta$ on $\\omega$ is weak (cf. two small examples in the 4th graph, showing flat and smooth densities)\n",
    "    * The likelihood graphs remain unchanged.\n",
    "    * The posterior now learns a lot about $\\theta$ and little is changed for $\\omega$\n",
    "* **Key point**: *Bayesian inference has the strongest impact on the aspects of the model that are least certain*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Strict prior on mint ($\\omega$), lax prior on coin ($\\theta$)\n",
    "![](fig-9.3.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# 1-mint, S-coins via grid approximation\n",
    "\n",
    "* Supports a **single group** experiment design: say we set people in some condition, and we check their performance; We want to estimate posterior model of performance applicable to general population.\n",
    "* We still have a single general tendency $\\omega$, but now multiple individual tendencies to respond ($\\theta_s$) -- the individual coin biases, or individual subject's predilections to perform.\n",
    "* The model is summarized in the following graph (and the same is shown in the PyMC3 code below).\n",
    "* We don't use the the PyMC3 model as we use grid approximation (directly computing the points in the posterior).  I include the model only so that it helps you in reading the graphs, and builds an intuition of PyMC3 model structures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# The graphical model\n",
    "![](fig-9.4.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "S = 2 # Two subjects in the same condition, we expose both to the same Boolean test\n",
    "K = 5  # concentration, how consistently subjects are expected to respond to the condition\n",
    "\n",
    "with pm.Model() as model3:\n",
    "    omega = pm.Beta (\"omega\", 2,2)\n",
    "    theta = [ pm.Beta (f\"theta_{s}\", alpha=omega*(K-2)+1, beta=(1-omega)*(K-2)+1) for s in np.arange(0,S) ]\n",
    "    y = [ pm.Bernoulli (f\"y_{s}\", p=theta[s]) for s in np.arange(0,S) ]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Analysis (the case with weak priors)\n",
    "\n",
    "* Typically in an experiment like that we will be interested in estimating $\\omega$ not the individual $\\theta_s$\n",
    "* The observations made for each of the subjects (coins) influence the overall posterior on $\\omega$\n",
    "* Now we have three parameters ($\\theta_1$, $\\theta_2$, and $\\omega$) -- it is no longer possible to plot things so easily in 3D (and in general you would have much more subjects than 2)\n",
    "* All parameters are still finite domain, and we can approximate them on a 3D grid.  Now we have an order of magnitude more grid points though.  Why?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "* In the model try to observe that:\n",
    "    * The coin biases are weakly dependent on $\\omega$. How can you see that?\n",
    "    * The prior on $\\omega$ is also fairly uninformed. How can you see that?\n",
    "    * Likelihoods are now computed for two different experiment results. Do you remember how are they computed?\n",
    "    * Why are likelihood contours vertical straight lines ?\n",
    "    * Why are the likelihood contours for the first coin tighter and more of them for the first coin?\n",
    "    * The marginal posteriors for $\\theta_i$ are roughly shifted towards the frequencies of successes in the the samples.\n",
    "    * The posterior for $\\omega$ is fairly inconclusive. Why do I say so?\n",
    "    \n",
    "\n",
    "![](fig-9.5.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# The case with strong priors (1-mint, S-coins)\n",
    "* In a hierarchical, model the estimate of each individual parameter simultanously informs as about parameters of others\n",
    "* This will be more visible in a case where the dependence of $\\theta_s$ on $\\omega$ is made strong. \n",
    "* Analyze the figure below:\n",
    "   * We still have a single prior for $\\omega$ which is fairly weak.\n",
    "   * Recall that we use the same prior for both $\\theta_s$.\n",
    "   * How can you see the strong dependence of both $\\theta_s$ on $\\omega$ in graphs? What parameter controls this dependence?\n",
    "   * Is the likelihood changed?\n",
    "   * Why are both $\\theta$ postierors so similar? And why is $\\theta_2$ shifted so much to the left comparing to the first model? (Note that the proportion in the data is 0.8)\n",
    "   * How has the posterior for $\\omega$ changed?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "* These figures are made with 50 points per parameter: 125 000 points which is very few, but add several more parameters and this gets out of hand.\n",
    "* Grid approximation is too inefficient for large models that appear in modern systems with many parameters.  \n",
    "\n",
    "![](fig-9.6.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# A Realistic Model with MCMC\n",
    "* In reality we often do not know what is the value of the parameter $K$ (so is the dependency of subject performance on the group performance strong or weak.)\n",
    "* But the choice of $K$ is important, it influences how parameters represent each other.\n",
    "* Intuitively, if subjects perform similarly we have evidence that $K$ is high, otherwise low.\n",
    "\n",
    "![](fig-9.7.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Gamma distribution\n",
    "\n",
    "* For $K$ we want a distribution that is not very informed, and only admits non-negative values\n",
    "\n",
    "$$\\Gamma (\\kappa \\mid s, r)$$ where $s$ is shape and $r$ is rate\n",
    "\n",
    "![](fig-9.8.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Gamma distribution (continued)\n",
    "\n",
    "* The mean of a $\\Gamma$-distribution is $\\mu = s/r$\n",
    "* The mode is $\\omega = (s-1)/r$ for $s > 1$ \n",
    "* The standard deviation is $\\sigma = \\sqrt{s}/r$\n",
    "* Consequently: $s = \\mu^2/\\sigma^2$ and $r = \\mu / \\sigma^2$ for $s > 0$\n",
    "* And the same derived from mode: \n",
    "$$s = 1 + \\omega r \\text{ and } r = \\frac{\\omega + \\sqrt{\\omega^2 + 4\\sigma^2}}{2\\sigma^2} \\text{ for mode } \\omega > 0$$\n",
    "* PDF for $\\Gamma(k)$ is complex, see https://en.wikipedia.org/wiki/Gamma_distribution (Chapter 9 does not show it)\n",
    "\n",
    "* The exponential distribution, Erlang distribution, and chi-squared distribution are special cases of the gamma distribution.\n",
    "* The PyMC3 API docs for Gamma distribution: https://docs.pymc.io/api/distributions/continuous.html#pymc3.distributions.continuous.Gamma\n",
    "    * The shape parameter is called 'alpha'\n",
    "    * The rate parameter is called 'beta'\n",
    "    * mean ('mu') and standard deviation ('sigma') are also directly supported so no need to use the book conversion functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "* This is the model of section 9.2.3 from the book expressed in PyMC3\n",
    "* Laregely reused for Therapeutic Touch example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "S = 2 # Two subjects in the same condition, we expose both to the same Boolean test\n",
    "      # For unclear reasons S is renamed to Ntotal when JAGS code is shown in the book\n",
    "      # This is the same thing (Ntotal = S)\n",
    "\n",
    "with pm.Model() as model4:\n",
    "    omega = pm.Beta (\"omega\", 1, 1) # Uniform, vague, non-commital\n",
    "    \n",
    "    # Recall that for mode,concentration parameterization of Beta distribution (Chapter 6), we need kappa > 2\n",
    "    # So we shift the Gamma distribution by two\n",
    "    # Alternative parameterization mean=1, sd=10\n",
    "    kappa = pm.Deterministic (\"kappa\", 2+pm.Gamma (\"kappaMinusTwo\", alpha=0.01, beta=0.01))\n",
    "    theta = [ \n",
    "        pm.Beta (f\"theta_{s}\", alpha=omega*(kappa-2)+1, beta=(1-omega)*(kappa-2)+1) \n",
    "        for s in np.arange(0,S) \n",
    "    ]\n",
    "    y = [ \n",
    "        pm.Bernoulli (f\"y_{s}\", p=theta[s]) \n",
    "        for s in np.arange(0,S) \n",
    "    ]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# The Therapeutic Touch example (9.2.4)\n",
    "\n",
    "* The purpose of the experiment is to test whether people can sense the presence of others without seeing them (that some kind of 'energy field' exists)\n",
    "* The experiment setup is in the image, the experimenter positions his hand close to the left or right hand of the subject, based on a coin toss.\n",
    "\n",
    "![](therapeutic-touch.jpg)\n",
    "\n",
    "(image from: https://www.skeptic.com/eskeptic/10-11-17/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "* 28 subjects, 10 trials each (more details in the book)\n",
    "* chance performance is 50% (random luck).  Question: Can people do better? So does their performance differ?\n",
    "    * More precisely: Do people do better than chance performance on average?\n",
    "    * More precisely: Can any individuals do better than chance performance?\n",
    "* This is a perfect match for our hierarchical model (with omega and concentration both being parameters, as we do not know much about this prior ability)\n",
    "* Data from the original experiment paper appears unimodal, so a Beta prior on $\\omega$ seems alrgight\n",
    "\n",
    "![](fig-9.9.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "* Let's build a pymc3 model for this problem"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "myData = pd.read_csv('TherapeuticTouchData.csv') # the dataset from the book's bundle\n",
    "IDs = myData['s'].unique()  # List of subjects                                                                                                                                                                                              \n",
    "S = len(IDs)  # Number of subjects     \n",
    "\n",
    "# Extract the different practitioners results  as individual arrays                                                                                                                                                                                                \n",
    "def f (cid):\n",
    "    return myData.loc[ myData['s'] == cid ]['y'].to_numpy()                                                                                                                                                                                         \n",
    "trials = np.transpose(np.asarray([f(s) for s in IDs], dtype=np.float32))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "* We are using PyMC3 vectorized variables (shape) to create 28 parameters $\\theta_s$ and the observed data variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "with pm.Model() as model5:\n",
    "    omega = pm.Beta (\"omega\", 1, 1)    \n",
    "    kappa = pm.Deterministic (\"kappa\", 2+pm.Gamma (\"kappaMinusTwo\", alpha=0.01, beta=0.01))\n",
    "    theta = pm.Beta (\"theta\", alpha=omega*(kappa-2)+1, beta=(1-omega)*(kappa-2)+1, shape=S) \n",
    "    y = pm.Bernoulli (\"y\", p=theta, observed=trials, shape=S) # the online line that changed from the previous model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with model5:    \n",
    "    trace = pm.sample(20000, tune=3000, target_accept=0.91) # step=pm.Metropolis())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pm.traceplot(trace, var_names=['omega', 'kappa', 'theta'])\n",
    "plt.show()                                                                                                                                                                                                                               \n",
    "az.summary (trace, var_names=['omega', 'kappa', 'theta'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1, 2, figsize=(10,4))\n",
    "pm.plot_posterior(\n",
    "    trace, var_names = [\"kappa\", \"omega\"], \n",
    "    point_estimate = 'mode', \n",
    "    credible_interval = 0.95,\n",
    "    ref_val = { 'omega' : [{ 'ref_val': 0.5 }] }, # a complex API to avoid giving ref_val for kappa\n",
    "    ax = ax)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "* Chance performance (0.5 for $\\omega$) is within 95% HDI, so we cannot rule it out\n",
    "* We use some helper functions to create the pair-wise comparison plots below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# small helper functions for plotting  differences\n",
    "def plot_diff (trace, i, j, ax):\n",
    "    pm.plot_posterior(\n",
    "        trace['theta'][:, i] - trace['theta'][:, j],\n",
    "        point_estimate = 'mode', \n",
    "        credible_interval = 0.95,\n",
    "        ref_val = 0.0,\n",
    "        round_to = 3,    \n",
    "        ax = ax)\n",
    "    ax.set_xlabel(f\"theta_{i} - theta_{j}\")\n",
    "    \n",
    "def plot_one (trace, i, ax):\n",
    "    pm.plot_posterior(\n",
    "        trace['theta'][:, i],\n",
    "        point_estimate = 'mode', \n",
    "        credible_interval = 0.95,\n",
    "        ref_val = 0.5,\n",
    "        round_to = 3,\n",
    "        ax = ax)\n",
    "    ax.set_xlabel (f\"theta_{i}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(3, 3, figsize=(15,14))\n",
    "plot_one (trace, 0, ax[0,0])\n",
    "plot_one (trace, 13, ax[1,1])\n",
    "plot_one (trace, 27, ax[2,2])\n",
    "plot_diff (trace, 0, 13, ax[0,1])\n",
    "plot_diff (trace, 0, 27, ax[0,2])\n",
    "plot_diff (trace, 13, 27, ax[1,2])\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "* I had some troubles plotting with pair_plot and plot_joint and the vectorized shape of theta, so I have given up.\n",
    "* Subjects are sorted by modes, so that 0 had the worst performance,  and subject 27 had the best performance.\n",
    "* Q. Can we conclude that the best performing subject is different from the worst performing one?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Shrinkage\n",
    "* Shrinkage: reduction of variance in low level estimators towards the modes of higher-level estimator for unimodal distributions.\n",
    "* For multi-modal distributions shrinkage means shifting towards higher-level models\n",
    "* In either case, this is the result of all variables influencing each other in this model."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# MLE\n",
    "\n",
    "MLE = Maximum Likelihood Estimation\n",
    "\n",
    "* The parameter values that maximize the likelihood of data (this is *roughly* what frequntist/classical statistics does)\n",
    "* Let's recall how is this computed:\n",
    "\n",
    "\\begin{align}\n",
    "p ( \\{y_{i\\mid s}\\} \\mid \\{\\theta_s\\}, \\omega, \\kappa ) = &\\\\ \n",
    "= & \\prod_s \\prod_{i \\mid s} p (y _{i|s} | \\theta_s, \\omega, \\kappa) \\\\\n",
    "= & \\prod_s \\prod_{i \\mid s} \\mathrm{bern} (y _{i|s} | \\theta_s) \\cdot \\mathrm{beta} (\\theta_s \\mid \\omega(\\kappa-2)+1, (1-\\omega)(\\kappa-2)+1)\n",
    "\\end{align}\n",
    "\n",
    "* It turns out that the value of $\\theta_s$ maximizing the likelihood is $(z_s/N_s)$ so the success rate of the subject's experiments\n",
    "* This value is pulled towards $\\omega$ when we want to maximize the overall model fit, dependeing on how strong is the prior."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "![](fig-9.12.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercises\n",
    "* Implement the model of section 9.5 in PyMC3 (the baseball players), probably the most interesting model in the chapter, but technically not much new is happening.\n",
    "* The above should be enough for the week, but Exercises 9.1 and 9.2 are also interesting, if you still have time (or you can look at them in the catch up week)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
