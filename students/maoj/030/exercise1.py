import numpy as np
from matplotlib import pyplot as plt

MAX_THETA = 1000

theta = np.linspace(0.,1.,MAX_THETA)

y = []
pTheta = np.array([np.zeros(200),np.linspace(1, 100, 50),np.linspace(100, 1, 50),np.zeros(400),np.linspace(1, 100, 50),np.linspace(100, 1, 50), np.zeros(200)]).flatten()

for e in pTheta:
    for item in e:
        y.append(item)

#Normalize it
pTheta = [(i/sum(y)) for i in y]


# # Taken from 020-probability:
# # Equation 5.11
def likelihood(z, N, theta):
    return (theta**z) * ((1-theta)**(N-z)) 


# Equation 5.8 (we use numpy operations on arrays for speed)   
def normalization_const_n (z, N): 
    return np.sum(np.multiply (likelihood (z,N,theta), pTheta))

# Equation 5.7
def posterior_n (z,N):
    denominator = normalization_const_n (z, N)
    numerator = np.multiply(likelihood (z, N, theta), pTheta)
    return numerator / denominator 

# Still some proportion missing, but not quite sure what. Ask Andrejz

res = posterior_n(14, 27)
fig, ax = plt.subplots(3, figsize=(8,20))
fig.suptitle('An example of where the prior distribution canno be expressed by a beta distribution')
fig.subplots_adjust(hspace = 0.8)


# both priors are the same (the only seem different in the book for an ad hoc reason?)
ax[0].set_title('Prior density θ graphed from def.')
ax[0].set_xlabel('θ')
ax[0].set_ylabel('p(θ)')
ax[0].bar(theta, pTheta, width=0.01)
ax[0].axis([-0.05, 1.05, -0.0001, 0.01])

# likelihoods
ax[1].set_title('Likelihood for z=14, N=27 based on Eq. 5.11')
ax[1].set_xlabel('θ')
ax[1].set_ylabel('p(D|θ)')
ax[1].bar(theta, likelihood(14, 27, theta), width=0.01)
ax[1].axis([-0.05, 1.05, -0.0001, 0.12])

ax[2].set_title('Prior density θ graphed from def.')
ax[2].set_xlabel('θ')
ax[2].set_ylabel('p(θ)')
ax[2].bar(theta, res, width=0.01)
ax[2].axis([-0.05, 1.05, -0.0001, 0.01])

plt.show()